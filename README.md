LolBot
======

A naff IRC bot I rattled off in a hurry once, for folks stuck behind censor
walls at work. Logs a channel and collects up all the URLs for later.
It was refactored for Twisted in 2011, then again to use pypi irc in 2015.
Has recently grown a quiz feature based on MoxQuizz, which might actually turn
into something useful one day.

Installation
------------

To get started, set up a Python virtual environment, and install the stuff in
`requirements.txt`.

    apt-get install python-virtualenv

Clone this git repository, and in the directory:

    virtualenv env
    env/bin/pip install -r requirements.txt

Then you can run the bot like so:

    env/bin/python lolbot.py --help

Development
-----------

Patches welcome! Development currently happens on the New Zealand Open Source
Society [GitLab](https://git.nzoss.org.nz/moxquizz-fans/lolbot).
